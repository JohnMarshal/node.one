var http = require('http');
var url = require('url');

// var server = http.createServer(function (req, res) {
//   res.writeHead(200, {'Content-Type': 'text/plain'});
//   res.end('Hello World\n');
// });
// server.listen(1337, '127.0.0.1');
// console.log('Server running at http://127.0.0.1:1337/');

function start (route, handle) {
	function onRequest (req, res) {
		// var postData = '';
		var pathname = url.parse(req.url).pathname;
		console.log('Request: ' + pathname);
		
		// req.setEncoding('utf8');

		// req.addListener('data', function(postDataChunk){
		// 	postData += postDataChunk;
		// 	console.log('POST data chunk: ' + postDataChunk);
		// });
		req.addListener('end', function(){
			// route(handle, pathname, res, postData);
			console.log('------------------------');
		});
		route(handle, pathname, res, req);
	}

	http.createServer(onRequest).listen(8888, '127.0.0.1');
	console.log('Server has started\n');
}

exports.start = start;