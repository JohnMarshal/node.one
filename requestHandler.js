// var exec = require('child_process').exec;
var consoleHelper = '----------------------';
var querystring = require('querystring'),
	fs = require('fs'),
	formidable = require("formidable");

function start (res) {
	console.log('Handle: Start');
	// console.log(consoleHelper);

	// exec("dir", function (error, stdout, stderr) {
	// 	res.writeHead(200, {"Content-Type": "text/plain"});
	// 	res.write(stdout);
	// 	console.log(error + ' ' + stdout + ' ' + stderr);
	// 	res.end();
	// });

	var body = '<html>'+
		'<head>'+
		'<meta http-equiv="Content-Type" content="text/html; '+
		'charset=UTF-8" />'+
		'</head>'+
		'<body>'+
		'<form action="/upload" enctype="multipart/form-data" method="post">'+
		'<input type="file" name="upload" />' + 
		'<input type="submit" value="Upload file" />'+
		'</form>'+
		'</body>'+
		'</html>';

	res.writeHead(200, {"Content-Type": "text/html"});
	res.write(body);
	res.end();
	
}

function upload (res, req) {
	console.log('Handle: Upload');

	var form  = new formidable.IncomingForm();
	console.log('parsing...');
	form.parse(req, function (error, fields, files) {
		console.log('parsing done');

		/* Possible error on Windows systems:
			tried to rename to an already existing file */
		fs.rename(files.upload.path, './tmp/test.png', function (err) {
			if (err) {
				fs.unlink('/tmp/test.png');
				console.log(files.upload.path);
				fs.rename(files.upload.path, '/tmp/test.png');
			}
		});

		// console.log(consoleHelper);
		res.writeHead(200, {"Content-Type": "text/html"});
		res.write('received image:<br/>');
		res.write('<img src="/show" />');
		res.end();
	});
}

function show (res) {
	console.log('Handle: Show');
	fs.readFile('./tmp/test.png', 'binary', function(error, file){
		if (error) {
			res.writeHead(500, {'Content-Type': 'text/plain'});
			res.write(error + '\n');
			res.end();
		} else {
			res.writeHead(200, {'Content-Type': 'image/png'});
			res.write(file, 'binary');
			res.end();
		};
	});
}

exports.start = start;
exports.upload = upload;
exports.show = show;

